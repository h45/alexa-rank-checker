<?php

 /*
 Tools Coded by ancode1337
 Email : hirarkyid@gmail.com
 */

 /*  
 Install php module :
 - php-xml
 - php-curl
 */

 class Zitools {
 	public $url,
         $list_url;

   // Color
   public $foreground_colors = [];
   
   public function __construct() {
    $this->foreground_colors['black'] = "\e[0;30m";
    $this->foreground_colors['dark_gray'] = "\e[1;30m";
    $this->foreground_colors['blue'] = "\e[0;34m";
    $this->foreground_colors['light_blue'] = "\e[1;34m";
    $this->foreground_colors['green'] = "\e[0;32m";
    $this->foreground_colors['light_green'] = "\e[1;32m";
    $this->foreground_colors['cyan'] = "\e[0;36m";
    $this->foreground_colors['light_cyan'] = "\e[1;36m";
    $this->foreground_colors['red'] = "\e[0;31m";
    $this->foreground_colors['light_red'] = "\e[1;31m";
    $this->foreground_colors['purple'] = "\e[0;35m";
    $this->foreground_colors['light_purple'] = "\e[1;35m";
    $this->foreground_colors['brown'] = "\e[0;33m";
    $this->foreground_colors['yellow'] = "\e[1;33m";
    $this->foreground_colors['light_gray'] = "\e[0;37m";
    $this->foreground_colors['white'] = "\e[1;37m";
  }
  
  /* ALEXA RANK MASS CHECKER */   	
  public function alexaRank($url) {
   $this->list_url = $url;
   @system('clear');
   $content = file_get_contents($this->list_url);
   $explode = explode("\n", $content);
   foreach( $explode as $massUrl ) {
     $curl = curl_init();
     curl_setopt($curl, CURLOPT_URL, "http://data.alexa.com/data?cli=10&dat=snbamz&url=".$massUrl);
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19");
     $exec = curl_exec($curl);
     curl_close($curl);	
     $xml = simplexml_load_string($exec);
     $result = json_decode(json_encode($xml), true);
     sleep(1);
     echo $this->foreground_colors['white'].strtoupper("url : ").$this->foreground_colors['light_cyan'].strtoupper($result['@attributes']['URL']).PHP_EOL;
     echo $this->foreground_colors['white'].strtoupper("reach : ").$this->foreground_colors['light_red'].$result['SD'][1]['REACH']['@attributes']['RANK'].PHP_EOL;    
     echo $this->foreground_colors['white'].strtoupper("global rank : ").$this->foreground_colors['light_green'].$result['SD'][1]['POPULARITY']['@attributes']['TEXT'].PHP_EOL;
     echo $this->foreground_colors['white'].strtoupper("local rank : ").$this->foreground_colors['yellow'].$result['SD'][1]['COUNTRY']['@attributes']['RANK'].PHP_EOL;
     echo $this->foreground_colors['white'].strtoupper("code country : ").$this->foreground_colors['light_gray'].$result['SD'][1]['COUNTRY']['@attributes']['CODE'].PHP_EOL;
     echo $this->foreground_colors['light_blue']."===============".PHP_EOL.$this->foreground_colors['white'];

   }
 } 	
}

 // ALEXA RANK MASS CHECKER {
$alexaRank = new Zitools();
@system("clear");
sleep(1);
if(!empty($argv[1])) {
$alexaRank->alexaRank($argv[1]);
}
else {
sleep(2);
echo	strtoupper("url : - ").PHP_EOL;
echo	strtoupper("reach : - ").PHP_EOL;
echo	strtoupper("global rank : - ").PHP_EOL;
echo	strtoupper("local rank : - ").PHP_EOL;
echo	strtoupper("code country : - ").PHP_EOL.PHP_EOL;
echo	strtoupper("run the program [✓] : ")."php alexa.php list.txt".PHP_EOL;
}
 // } 
?>